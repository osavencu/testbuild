
# GCP API Controller
### Server URL:
**http://gcp-api-ctrl.stagingenv.live:5000**

### Features:

  - list all instances for a GCP project
  - start/stop instance
  - get instance status
  - get cpu utilisation
  - get gpu data



### API Details

WARNING: These api endpoints are subject to change..


|       ENDPOINT |          BODY                 |            COMMENTS         |
|----------------|-------------------------------|-----------------------------|
| /list-vms/project_name/zone | empty |no|
| /change-vm-state | {"project": "**project_name**","zone": "**instance_zone**","name": "**instance_name**","operation":"**operation**"} | **operation** can be: `start` or `stop` |
| /all-vms-cpu/project_name/metric_type | empty | **metric_type**: `reserved_cores` `utlization` |
| /all-vms-gpu/project_name/metric_type | empty | **metric_type**: `max_gpu_utilization` `max_gpu_memory` `avg_gpu_utilization` `avg_gpu_memory`|
| /check-vm-status/project_name/zone/vm | empty | no |



### How to monitor the GPU

- You need to activate  [Stackdrive](https://app.google.stackdriver.com) for your GCP project
- ssh to the instance that you want to monitor
- copy [gpu_monitoring.py](https://github.com/LaifScience/gcp-api-ctrl/blob/master/gpu_monitoring.py "gpu_monitoring.py") to your instance.
	- this script requires **google-cloud-monitoring** library
- run the script with: `python /path/to/gpu_monitoring.py project_name instance_name >> /var/log/gpu.log 2>&1`
#### Initialise virtualenv to run script:
```sh
$ pip install virtualenv
$ virtualenv <your-env>
$ source <your-env>/bin/activate
$ <your-env>/bin/pip install google-cloud-monitoring
```
### [Cron Job](https://crontab.guru/every-1-minute)

#### How to:

Because you need to constantly monitor the gpu, you should create a cron job that calls the script once a minute.

Make the script executable by:
`chmod u+x /path/to/script.py`

Note that, you need a shebang (i.e. indicate interpreter in the first line of the script). 

For python2:

`#!/usr/bin/env python2`

For python3:

`#!/usr/bin/env python3`

Open your cron table by

`crontab -e`

Add the following cron entry:

`* * * * * /path/to/script.py`

Note that, if the script is not executable you can indicate the interpreter on the go:

For python2:

`* * * * * /usr/bin/env python2 /path/to/script.py`

For python3:

`* * * * * /usr/bin/env python3 /path/to/script.py`
