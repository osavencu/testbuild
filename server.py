import logging

logging.basicConfig(format='%(levelname)s:%(threadName)s:%(filename)s:%(funcName)s:%(message)s', level=logging.INFO)
logging.getLogger('googleapicliet.discovery_cache').setLevel(logging.ERROR)
logging.getLogger('googleapiclient.discovery').setLevel(logging.CRITICAL)
logging.getLogger('googleapiclient.discovery_cache').setLevel(logging.ERROR)
import argparse
import time
import datetime
import json
import pprint
import googleapiclient.discovery
from flask import Flask
from flask import jsonify
from flask import request
import googleapiclient.discovery
from google.cloud import monitoring_v3
from google.oauth2 import service_account

app = Flask(__name__)


@app.route('/')
def index():
    return 'Server Works!'


def get_service_account_auth_file_path(project_name):
    return {
        'trainsfer-app': 'trainsfer-app-73ed68e30dd8.json',
        'nocodeai': 'nocodeai-c16dbb5ffc69.json'
    }.get(project_name, 'add-new-project')


def format_rfc3339(datetime_instance):
    """Formats a datetime per RFC 3339."""
    return datetime_instance.isoformat("T") + "Z"


def get_start_time():
    """ Returns the start time for the 5-minute window to read the custom
    metric from within.
    :return: The start time to begin reading time series values, picked
    arbitrarily to be an hour ago and 5 minutes
    """
    # Return an hour ago - 5 minutes
    start_time = (datetime.datetime.utcnow() - datetime.timedelta(minutes=5))
    print(start_time)
    return format_rfc3339(start_time)


def get_end_time():
    """ Returns the end time for the 5-minute window to read the custom metric
    from within.
    :return: The start time to begin reading time series values, picked
    arbitrarily to be an hour ago, or 5 minutes from the start time.
    """
    end_time = datetime.datetime.utcnow() - datetime.timedelta(minutes=0)
    print(end_time)
    return format_rfc3339(end_time)


@app.route('/list-vms/<project_name>/<zone>', methods=['GET'])
def list_instances(project_name, zone):
    credentials = service_account.Credentials.from_service_account_file(
        get_service_account_auth_file_path(project_name), scopes=['https://www.googleapis.com/auth/compute'])
    compute = googleapiclient.discovery.build('compute', 'v1', credentials=credentials)
    response = compute.instances().list(project=project_name, zone=zone).execute()
    return jsonify(response['items']) if 'items' in response else None


@app.route('/change-vm-state', methods=['POST'])
def change_vm_state():
    vm_info = request.json
    print(vm_info)
    credentials = service_account.Credentials.from_service_account_file(
        get_service_account_auth_file_path(vm_info['project']), scopes=['https://www.googleapis.com/auth/compute'])
    compute = googleapiclient.discovery.build('compute', 'v1', credentials=credentials)

    if 'start' == vm_info['operation']:
        result = compute.instances().start(project=vm_info['project'], zone=vm_info['zone'],
                                           instance=vm_info['name']).execute()
    else:
        result = compute.instances().stop(project=vm_info['project'], zone=vm_info['zone'],
                                          instance=vm_info['name']).execute()
    return jsonify(result)


@app.route('/all-vms-cpu/<project_name>/<metric_type>', methods=['GET'])
def check_cpu(project_name, metric_type):
    credentials = service_account.Credentials.from_service_account_file(
        get_service_account_auth_file_path(project_name), scopes=['https://www.googleapis.com/auth/compute'])
    client = googleapiclient.discovery.build('monitoring', 'v3', credentials=credentials)

    metric_client = monitoring_v3.MetricServiceClient()
    proj_name = metric_client.project_path(project_name)

    # TODO: generate more clear data
    results_dict = client.projects().timeSeries().list(
        name=proj_name,
        filter='metric.type = "compute.googleapis.com/instance/cpu//{}"'.format(metric_type),
        pageSize=20,
        interval_startTime=get_start_time(),
        interval_endTime=get_end_time()).execute()

    print('list_timeseries response:\n{}'.format(pprint.pformat(results_dict)))
    return json.dumps(results_dict)


@app.route('/all-vms-gpu/<project_name>/<metric_type>', methods=['GET'])
def check_gpu(project_name, metric_type):
    credentials = service_account.Credentials.from_service_account_file(
        get_service_account_auth_file_path(project_name), scopes=['https://www.googleapis.com/auth/monitoring'])
    client = googleapiclient.discovery.build('monitoring', 'v3', credentials=credentials)

    metric_client = monitoring_v3.MetricServiceClient()
    proj_name = metric_client.project_path(project_name)

    # TODO: generate more clear data
    results_dict = client.projects().timeSeries().list(
        name=proj_name,
        filter='metric.type = "custom.googleapis.com/{}"'.format(metric_type),
        pageSize=20,
        interval_startTime=get_start_time(),
        interval_endTime=get_end_time()).execute()

    print('list_timeseries response:\n{}'.format(pprint.pformat(results_dict)))
    return json.dumps(results_dict)


@app.route('/check-vm-status/<project_name>/<zone>/<vm>', methods=['GET'])
def check_status(project_name, zone, vm):
    credentials = service_account.Credentials.from_service_account_file(
        get_service_account_auth_file_path(project_name), scopes=['https://www.googleapis.com/auth/monitoring'])
    compute = googleapiclient.discovery.build('compute', 'v1', credentials=credentials)
    response = compute.instances().get(project=project_name, zone=zone, instance=vm).execute()
    return jsonify(response)


def create_custom_metric(project_id, custom_metric_type, metric_kind):
    client = monitoring_v3.MetricServiceClient()
    """Create custom metric descriptor"""
    metrics_descriptor = {
        "type": custom_metric_type,
        "labels": [
            {
                "key": "environment",
                "valueType": "STRING",
                "description": "An arbitrary measurement"
            },
            {
                "key": "environment",
                "valueType": "STRING",
                "description": "An arbitrary measurement"
            },
            {
                "key": "environment",
                "valueType": "STRING",
                "description": "An arbitrary measurement"
            }
        ],
        "metricKind": metric_kind,
        "valueType": "INT64",
        "unit": "items",
        "description": "An arbitrary measurement.",
        "displayName": "Custom Metric"
    }

    return client.projects().metricDescriptors().create(
        name=project_id, body=metrics_descriptor).execute()


if __name__ == '__main__':
    parser = argparse.ArgumentParser(add_help=True)
    parser.add_argument('--port', type=int, default=80)

    args = parser.parse_args()

    logging.info("Starting Server.")

    app.run(host='0.0.0.0', port=args.port)
