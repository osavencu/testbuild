from subprocess import Popen, PIPE
import os
import time
import sys


# get data out of: nvidia-smi --query-gpu=utilization.gpu,utilization.memory --format=csv,noheader,nounits
# the result is formed out of 2 percentage values
def compute_stats():
    all_gpu = []
    all_mem = []
    for i in range(10):
        p = Popen(["nvidia-smi", "--query-gpu=utilization.gpu,utilization.memory", "--format=csv,noheader,nounits"],
                  stdout=PIPE)
        stdout, stderror = p.communicate()
        output = stdout.decode('UTF-8')
        # Split on line break
        lines = output.split(os.linesep)
        numDevices = len(lines) - 1
        gpu = []
        mem = []
        for g in range(numDevices):
            line = lines[g]
            # print(line)
            vals = line.split(', ')
            # print(vals)
            gpu.append(float(vals[0]))
            mem.append(float(vals[1]))

        all_gpu.append(gpu)
        all_mem.append(mem)
        time.sleep(1)

    max_gpu = [max(x[i] for x in all_gpu) for i in range(numDevices)]
    avg_gpu = [sum(x[i] for x in all_gpu) / len(all_gpu) for i in range(numDevices)]
    max_mem = [max(x[i] for x in all_mem) for i in range(numDevices)]
    avg_mem = [sum(x[i] for x in all_mem) / len(all_mem) for i in range(numDevices)]
    return max_gpu, avg_gpu, max_mem, avg_mem


def get_process_info():
    all_info = ''
    p = Popen(["nvidia-smi", "--query-compute-apps=pid,process_name", "--format=csv,noheader,nounits"], stdout=PIPE)
    stdout, stderror = p.communicate()
    output = stdout.decode('UTF-8')
    # Split on line break
    lines = output.split(os.linesep)
    for i in range(len(lines) - 1):
        procdataline = lines[i]
        procdata = procdataline.split(', ')
        all_info += str(procdata[1]).replace('./', '') + ' - ' + str(procdata[0] + ' | ')

    return all_info


from google.cloud import monitoring_v3

if len(sys.argv) < 4:
    print("You need to pass the project, instance name and zone as arguments")
    sys.exit(1)

client = monitoring_v3.MetricServiceClient()
project = sys.argv[1]
project_name = client.project_path(project)
instance_zone = sys.argv[3]


def write_time_series(name, gpu_nr, value, proc_info):
    series = monitoring_v3.types.TimeSeries()
    series.metric.type = 'custom.googleapis.com/' + name
    series.metric.labels['process_info'] = proc_info
    series.resource.type = 'gce_instance'
    series.resource.labels['zone'] = instance_zone
    series.resource.labels['instance_id'] = sys.argv[2] + "_gpu_" + str(gpu_nr)

    point = series.points.add()
    point.value.double_value = value
    now = time.time()
    point.interval.end_time.seconds = int(now)
    point.interval.end_time.nanos = int(
        (now - point.interval.end_time.seconds) * 10 ** 9)
    client.create_time_series(project_name, [series])


try:
    max_gpu, avg_gpu, max_mem, avg_mem = compute_stats()
    process_info = get_process_info()
    if not process_info.strip():
        process_info = 'no info'
    for i in range(len(max_gpu)):
        write_time_series('max_gpu_utilization', i, max_gpu[i], process_info)
        write_time_series('max_gpu_memory', i, max_mem[i], process_info)
        write_time_series('avg_gpu_utilization', i, avg_gpu[i], process_info)
        write_time_series('avg_gpu_memory', i, avg_mem[i], process_info)
except Exception as e:
    print(e)
