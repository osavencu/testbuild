FROM python:buster

RUN apt-get update
RUN apt-get install -y python3-tk cmake git libsm6 libxext6

RUN pip3 install --upgrade pip \
      && pip3 install flask \
      && pip3 install matplotlib \
      && pip3 install google-api-python-client \
      && pip3 install google-auth \
      && pip3 install google-cloud-monitoring \
      && pip3 install google-auth-httplib2 \
      && pip3 install requests

RUN mkdir -p /code
WORKDIR /code

ADD *.py /code/
ADD *.json /code/
CMD ["python", "-u", "server.py", "--port", "5000"]